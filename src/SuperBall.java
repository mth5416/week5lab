/**
 * SuperBall.java
 * A ball that bounces back and forth on the screen and changes colors
 */

import wheelsunh.users.*;
import java.awt.Color;

public class SuperBall extends ShapeGroup implements Animator {

    //Variable declaration
    protected Ellipse[] ballLayers;
    protected int count, startX, startY, scale;
    protected int[][] direction;
    protected int[][] rgb;
    protected int xVel, yVel;
    protected Rectangle back;

    /**
     * Default constructor
     */
    public SuperBall(){
        //All the variables
        super();
        startX = 16;
        startY = 0;
        count = 64;
        scale = 4;
        xVel = 0;
        yVel = 0;
        rgb = new int[count][];
        direction = new int[count][];
        //Sets up color and direction matricies
        for(int i=0; i<rgb.length; i++){
            rgb[i] = new int[3];
            rgb[i][0] = 128;
            rgb[i][1] = 11;
            rgb[i][2] = 128;
            direction[i] = new int[3];
            direction[i][0] = -1;
            direction[i][1] = 1;
            direction[i][2] = 1;
        }

        //Creates the super cool background
        back = new Rectangle();
        back.setLocation(0,0);
        back.setSize(700,500);
        back.setColor(new Color(rgb[0][0],rgb[0][1],rgb[0][2]));
        spreadColor();

        //Sets up all the layers of the ball for the rainbow effect
        ballLayers = new Ellipse[count];

        for(int i = ballLayers.length-1; i >= 0; i-=1){
            ballLayers[i] = new Ellipse();
            ballLayers[i].setLocation(startX + scale*(ballLayers.length-i)/2, startY + scale*(ballLayers.length-i)/2);
            ballLayers[i].setSize(scale*i,scale*i);
            ballLayers[i].setColor(new Color(rgb[i][0],rgb[i][1],rgb[i][2]));
            super.add(ballLayers[i]);
        }
    }

    /**
     * Used by the constructor to update starting values for colors
     */
    protected void spreadColor(){
        for(int i=0; i<(rgb.length); i++){
            for(int j=0; j<16*i; j++){
                updateColor(rgb[i],direction[i]);
            }
        }
    }

    /**
     * Used by the animate method to cycle the colors of all ellipses
     */
    protected void updateColor(){
        for(int i=0; i<rgb.length; i++){
            for(int j=0; j<rgb[i].length; j++){
                if(rgb[i][j] < 1 ){
                    direction[i][j] *= -1;
                    rgb[i][j] = 0;
                }
                if(rgb[i][j] > 254){
                    direction[i][j] *= -1;
                    rgb[i][j] = 255;
                }
                rgb[i][j] += direction[i][j];
                ballLayers[i].setColor(new Color(rgb[i][0],rgb[i][1],rgb[i][2]));
            }
        }

    }

    /**
     * Used by the spread color function to cycle through colors
     * @param rgb int[]
     * @param direction int[]
     */
    protected void updateColor(int[]rgb, int[] direction){
        for(int j=0; j<rgb.length; j++){
            if(rgb[j] < 1 || rgb[j] > 254){
                direction[j] *= -1;
                if(rgb[j] < 0) rgb[j] = 0;
                if(rgb[j] > 255) rgb[j] = 255;
            }
            rgb[j] += direction[j];
        }

    }

    /**
     * Main function to test SuperBall
     * @param args
     */
    public static void main(String[] args) {
        new Frame();
        SuperBall please = new SuperBall();
        AnimationTimer timer = new AnimationTimer(10,please);
        timer.start();
    }

    /**
     * Animates the ellipses and updates the color and position
     */
    @Override
    public void animate() {
        back.setColor(new Color(rgb[count-1][0],rgb[count-1][1],rgb[count-1][2]));
        updateColor();
        int xCheck = super.getXLocation() + super.getWidth();
        int yCheck = super.getYLocation() + super.getHeight();
        if(xCheck > 700 || super.getXLocation() < 0){
            xVel *= -1;
        }
        if(yCheck > 500 || super.getYLocation() < 0){
            yVel *= -1;
        }
        super.setLocation(super.getXLocation()+xVel, super.getYLocation()+yVel);
    }
}
